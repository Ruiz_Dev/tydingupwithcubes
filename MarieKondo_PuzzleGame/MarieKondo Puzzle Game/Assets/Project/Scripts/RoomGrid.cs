﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum squareState { Not, Empty, Player, Good, Bad, Wall }

public class RoomGrid : MonoBehaviour {

    public GameObject squarePrefab; //necessary to instantiate the squares of the grid
    public GameObject objectPrefab; //necessary to instantiate the objects of the level
    public GameObject noJoyObjectPrefab;
    public GameObject trashPrefab; //necessary to instantiate the trash square of the level
    public GameObject playerPrefab;
    public GameObject winCanvas;

    public enum LevelToPlay { one, two, three, four, five }
    public LevelToPlay level;

    public static int[] levelData;
    public static int levelSize;
    public static int trashIndex;

    
    public static int goalNumber = 0;
    [HideInInspector]
    public int numberOfMovements; //movements or acions?

    // Use this for initialization
    void Start()
    {
        if (level == LevelToPlay.one) //level 1
        {
            levelSize = Levels.level1Size;
            levelData = new int[levelSize * levelSize];
            for (int i = 0; i < levelSize * levelSize; i++)
            {
                levelData[i] = Levels.level1[i];
            }
        }
        else if (level == LevelToPlay.two) //level 2
        {
            //levelData = Levels.level2;
            levelSize = Levels.level2Size;
            levelData = new int[levelSize * levelSize];
            for(int i = 0; i < levelSize * levelSize; i++)
            {
                levelData[i] = Levels.level2[i];
            }
        }
        else if(level == LevelToPlay.three)
        {
            levelSize = Levels.level3Size;
            levelData = new int[levelSize * levelSize];
            for(int i = 0; i < levelSize* levelSize; i++)
            {
                levelData[i] = Levels.level3[i];
            }
        }
        else if(level == LevelToPlay.four)
        {
            levelSize = Levels.level4Size;
            levelData = new int[levelSize * levelSize];
            for(int i = 0; i < levelSize * levelSize; i++)
            {
                levelData[i] = Levels.level4[i];
            }
        }
        else if(level == LevelToPlay.five)
        {
            levelSize = Levels.level5Size;
            levelData = new int[levelSize * levelSize];
            for(int i = 0; i < levelSize * levelSize; i++)
            {
                levelData[i] = Levels.level5[i];
            }
        }

        drawGrid(levelData, levelSize);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void drawGrid(int[] level, int size) //size is Levels.levelSize
    {
        for (int i = 0; i < level.Length; ++i)
        {
            int column = i % size;
            int row = i / size;

            if (level[i] != 0)
            {
                Vector3 squarePosition = new Vector3(column, -squarePrefab.transform.lossyScale.y / 2.0f, -row);

                //place square in world
                Instantiate(squarePrefab, squarePosition, Quaternion.identity);

                //draw objects
                switch (level[i])
                {
                    case 2:
                        Instantiate(playerPrefab, new Vector3(squarePosition.x, 0.5f, squarePosition.z),Quaternion.identity);
                        break;
                    case 3:
                        Instantiate(objectPrefab, new Vector3(squarePosition.x, 0.5f, squarePosition.z), Quaternion.identity);
                        break;
                    case 4:
                        Instantiate(noJoyObjectPrefab, new Vector3(squarePosition.x, 0.5f, squarePosition.z), Quaternion.identity);
                        ++goalNumber;
                        break;
                    case 5:
                        Instantiate(trashPrefab, new Vector3(squarePosition.x, 0f, squarePosition.z), Quaternion.identity);
                        trashIndex = i;
                        level[i] = 1;
                        break;
                }
            }
        }
    }
}
