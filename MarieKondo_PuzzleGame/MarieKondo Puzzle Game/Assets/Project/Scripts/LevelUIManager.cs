﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelUIManager : MonoBehaviour {

    public GameObject panelWin;

    public void resetScene()
    {
        string currentScene = SceneManager.GetActiveScene().name;
        RoomGrid.goalNumber = 0;
        SceneManager.LoadScene(currentScene);
    }

    public void goToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void WinState()
    {
        if (SceneManager.GetActiveScene().name == "level05") SceneManager.LoadScene(0);
        else SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
