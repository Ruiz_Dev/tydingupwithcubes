﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

  //  private Vector3 offset = new Vector3(0,0,0.5f);
    public GameObject center;
    public GameObject up;
    public GameObject down;
    public GameObject left;
    public GameObject right;

    public int step = 9;
    public float speed = 0.01f;

    [HideInInspector]
    public bool input = true;

    [HideInInspector]
    public int currentPositionIndex;

    [HideInInspector]
    public bool haveObject;
    [HideInInspector]
    public char handOrientation = 's'; //south, east, north, west, up, down
    [HideInInspector]
    public int objectDataGrid;

    [HideInInspector] public bool winner = false;

	void Start () {

        haveObject = false;
        currentPositionIndex = RoomGrid.levelSize * -Mathf.RoundToInt(transform.position.z) + Mathf.RoundToInt(transform.position.x);
    }
	

	void Update ()
    {
        if (!winner)
        {
            if (input)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    if (canMoveToSquareRight())
                    {
                        StartCoroutine(moveRight());
                        input = false;

                    }
                    else
                    {
                        //feedback
                    }


                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    if (canMoveToSquareLeft())
                    {
                        StartCoroutine(moveLeft());
                        input = false;
                    }
                    else
                    {
                        //feedback
                    }

                }
                else if (Input.GetKey(KeyCode.UpArrow))
                {
                    if (canMoveToSquareForward())
                    {
                        StartCoroutine(moveUp());
                        input = false;
                    }
                    else
                    {
                        //feedback
                    }

                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    if (canMoveToSquareBack())
                    {
                        StartCoroutine(moveDown());
                        input = false;
                    }
                    else
                    {
                        //feedback
                    }

                }
            }
        }
	}

    IEnumerator moveUp()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            transform.RotateAround(up.transform.position, Vector3.right, step);
            yield return new WaitForSeconds(speed);
        }
        center.transform.position = transform.position;

        //grid update
        RoomGrid.levelData[currentPositionIndex] = 1;
        currentPositionIndex -= RoomGrid.levelSize;
        RoomGrid.levelData[currentPositionIndex] = 2;
        input = true;
        calculateNewHandOrientation('f');
        //printLevel();
    }

    IEnumerator moveDown()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            transform.RotateAround(down.transform.position, Vector3.left, step);
            yield return new WaitForSeconds(speed);
        }
        center.transform.position = transform.position;
        //grid update
        RoomGrid.levelData[currentPositionIndex] = 1;
        currentPositionIndex += RoomGrid.levelSize;
        RoomGrid.levelData[currentPositionIndex] = 2;
        input = true;
        calculateNewHandOrientation('b');
        //printLevel();
    }

    IEnumerator moveRight()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            transform.RotateAround(right.transform.position, Vector3.back, step);
            yield return new WaitForSeconds(speed);
        }
        center.transform.position = transform.position;

        //grid update
        RoomGrid.levelData[currentPositionIndex] = 1;
        currentPositionIndex += 1;
        RoomGrid.levelData[currentPositionIndex] = 2;
        input = true;
        calculateNewHandOrientation('r');
        //printLevel();
    }

    IEnumerator moveLeft()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            transform.RotateAround(left.transform.position, Vector3.forward, step);
            yield return new WaitForSeconds(speed);
        }
        center.transform.position = transform.position;

        //grid update
        RoomGrid.levelData[currentPositionIndex] = 1;
        currentPositionIndex -= 1;
        RoomGrid.levelData[currentPositionIndex] = 2;
        input = true;
        calculateNewHandOrientation('l');
        //printLevel();
    }

    #region CAN MOVE DIRECTION
    private bool canMoveToSquareRight()
    {
        int column = currentPositionIndex % RoomGrid.levelSize;
        int row = currentPositionIndex / RoomGrid.levelSize;
        if (column != RoomGrid.levelSize - 1) //si no esta a la derecha de la grid
        {
            if (RoomGrid.levelData[currentPositionIndex + 1] == 1)//si la proxima casilla esta vacia
            {
                if (haveObject)
                {
                    if (handOrientation == 'e') return false;
                    else if (handOrientation == 'n')
                    {
                        if (row == 0) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx + 1];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'w') return true;
                    else if(handOrientation == 's')
                    {
                        if (row == RoomGrid.levelSize - 1) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx + 1];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'u')
                    {
                        int futureColumn = (currentPositionIndex + 1) % RoomGrid.levelSize;
                        if (futureColumn == RoomGrid.levelSize - 1) return true;
                        int futObjSquare = RoomGrid.levelData[currentPositionIndex + 2];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                }
                else return true; //if dont have object enough checking

            }
        }
        
        return false;

    }

    private bool canMoveToSquareLeft()
    {
        int column = currentPositionIndex % RoomGrid.levelSize;
        int row = currentPositionIndex / RoomGrid.levelSize;
        if (column != 0) //si no esta a la izquierda de la grid
        {
            if (RoomGrid.levelData[currentPositionIndex - 1] == 1)//si la proxima casilla esta vacia
            {
                if (haveObject)
                {
                    if (handOrientation == 'w') return false;
                    else if (handOrientation == 'n')
                    {
                        if (row == 0) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx - 1];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'e') return true;
                    else if (handOrientation == 's')
                    {
                        if (row == RoomGrid.levelSize - 1) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx - 1];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'u')
                    {
                        int futureColumn = (currentPositionIndex - 1) % RoomGrid.levelSize;
                        if (futureColumn == 0) return true;
                        int futObjSquare = RoomGrid.levelData[currentPositionIndex - 2];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                }
                else return true; //if dont have object enough checking

            }
        }

        return false;
    }

    private bool canMoveToSquareForward()
    {
        int column = currentPositionIndex % RoomGrid.levelSize;
        int row = currentPositionIndex / RoomGrid.levelSize;
        if (row != 0) //si no esta arriba de la grid
        {
            if (RoomGrid.levelData[currentPositionIndex - RoomGrid.levelSize] == 1)//si la proxima casilla esta vacia
            {
                if (haveObject)
                {
                    if (handOrientation == 'n') return false;
                    else if (handOrientation == 'w')
                    {
                        if (column == 0) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx -RoomGrid.levelSize];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 's') return true;
                    else if (handOrientation == 'e')
                    {
                        if (column == RoomGrid.levelSize - 1) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx - RoomGrid.levelSize];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'u')
                    {
                        int futureRow = (currentPositionIndex - RoomGrid.levelSize) / RoomGrid.levelSize;
                        if (futureRow == 0) return true;
                        int futObjSquare = RoomGrid.levelData[currentPositionIndex - (RoomGrid.levelSize*2)];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                }
                else return true; //if dont have object enough checking

            }
        }

        return false;
    }

    private bool canMoveToSquareBack()
    {
        int column = currentPositionIndex % RoomGrid.levelSize;
        int row = currentPositionIndex / RoomGrid.levelSize;
        if (row != RoomGrid.levelSize-1) //si no esta arriba de la grid
        {
            if (RoomGrid.levelData[currentPositionIndex + RoomGrid.levelSize] == 1)//si la proxima casilla esta vacia
            {
                if (haveObject)
                {
                    if (handOrientation == 's') return false;
                    else if (handOrientation == 'w')
                    {
                        if (column == 0) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx + RoomGrid.levelSize];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'n') return true;
                    else if (handOrientation == 'e')
                    {
                        if (column == RoomGrid.levelSize - 1) return true;
                        gameObject.GetComponent<HandMechanic>().updateObjIdx();
                        int objIdx = gameObject.GetComponent<HandMechanic>().objectIdx;
                        int futObjSquare = RoomGrid.levelData[objIdx + RoomGrid.levelSize];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                    else if (handOrientation == 'u')
                    {
                        int futureRow = (currentPositionIndex + RoomGrid.levelSize) / RoomGrid.levelSize;
                        if (futureRow == RoomGrid.levelSize-1) return true;
                        int futObjSquare = RoomGrid.levelData[currentPositionIndex + (RoomGrid.levelSize * 2)];
                        if (futObjSquare == 0 || futObjSquare == 1 || futObjSquare == 5) return true;
                    }
                }
                else return true; //if dont have object enough checking

            }
        }

        return false;
    }
    #endregion 

    private void calculateNewHandOrientation(char dir)
    {
        if(dir == 'f')
        {
            switch (handOrientation)
            {
                case 'n':
                    handOrientation = 'd';
                    break;
                case 'e':
                    handOrientation = 'e';
                    break;
                case 's':
                    handOrientation = 'u';
                    break;
                case 'w':
                    handOrientation = 'w';
                    break;
                case 'u':
                    handOrientation = 'n';
                    break;
                case 'd':
                    handOrientation = 's';
                    break;
            }
        }
        else if(dir == 'r')
        {
            switch (handOrientation)
            {
                case 'n':
                    handOrientation = 'n';
                    break;
                case 'e':
                    handOrientation = 'd';
                    break;
                case 's':
                    handOrientation = 's';
                    break;
                case 'w':
                    handOrientation = 'u';
                    break;
                case 'u':
                    handOrientation = 'e';
                    break;
                case 'd':
                    handOrientation = 'w';
                    break;
            }
        }
        else if(dir == 'b')
        {
            switch (handOrientation)
            {
                case 'n':
                    handOrientation = 'u';
                    break;
                case 'e':
                    handOrientation = 'e';
                    break;
                case 's':
                    handOrientation = 'd';
                    break;
                case 'w':
                    handOrientation = 'w';
                    break;
                case 'u':
                    handOrientation = 's';
                    break;
                case 'd':
                    handOrientation = 'n';
                    break;
            }
        }
        else if(dir == 'l')
        {
            switch (handOrientation)
            {
                case 'n':
                    handOrientation = 'n';
                    break;
                case 'e':
                    handOrientation = 'u';
                    break;
                case 's':
                    handOrientation = 's';
                    break;
                case 'w':
                    handOrientation = 'd';
                    break;
                case 'u':
                    handOrientation = 'w';
                    break;
                case 'd':
                    handOrientation = 'e';
                    break;
            }
        }
    }

    public void printLevel()
    {
        Debug.Log("Level State: --------------");
        for(int i = 0; i < RoomGrid.levelSize; ++i)
        {
            string rowInfo = "   ";
            for(int j = 0; j < RoomGrid.levelSize; ++j)
            {
                rowInfo += RoomGrid.levelData[i * RoomGrid.levelSize + j] + " ";
                
            }
            Debug.Log(rowInfo);
            rowInfo = "   ";
        }
        Debug.Log("---------------------------");
    }
}
