﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour {

    public Button[] lvlButtons;
    
    void Start()
    {
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);//quan es guany un nivell PlayerPrefs.SetInt("levelReached", levelTOunLock)
        for(int i = 0; i < lvlButtons.Length; i++)
        {
            if(i + 1 > levelReached)
            { 
                lvlButtons[i].interactable = false;
            }
        }
    }
    public void SelectLvl(string lvlName)
    {
        SceneManager.LoadScene(lvlName);
    }
}
