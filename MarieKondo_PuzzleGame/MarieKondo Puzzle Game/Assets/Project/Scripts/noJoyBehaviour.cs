﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noJoyBehaviour : MonoBehaviour {

    public bool isInTrash()
    {
        int ind = -Mathf.RoundToInt(transform.position.z) * RoomGrid.levelSize + Mathf.RoundToInt(transform.position.x);
        //Debug.Log("position: " + ind);
        return ind==RoomGrid.trashIndex;
    }

    public void getTrashed()
    {
        //animation
        //sound

        int ind = -Mathf.RoundToInt(transform.position.z) * RoomGrid.levelSize + Mathf.RoundToInt(transform.position.x);
        RoomGrid.levelData[ind] = 1;
        //destroy
        Destroy(gameObject);
        --RoomGrid.goalNumber;
        
    }
}
