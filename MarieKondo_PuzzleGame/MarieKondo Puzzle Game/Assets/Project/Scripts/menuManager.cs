﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuManager : MonoBehaviour {

    public GameObject[] menuButtons;
    int selectedID = 0;
    Color selectedColor = new Color(0.8039f,0.7058f, 0.1490f);
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            menuButtons[selectedID].GetComponent<Text>().color = Color.white;
            selectedID++;
            if (selectedID == 4) selectedID = 0;
            menuButtons[selectedID].GetComponent<Text>().color = selectedColor;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            menuButtons[selectedID].GetComponent<Text>().color = Color.white;
            selectedID--;
            if (selectedID == -1) selectedID = 3;
            menuButtons[selectedID].GetComponent<Text>().color = selectedColor;
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            if(selectedID == 0)
            {
                PlayButton();
            }else if(selectedID == 3)
            {
                exitGame();
            }else if(selectedID == 1)
            {
                goToSettings();
            }
        }
	}

    public void PlayButton()
    {
        SceneManager.LoadScene(2);
    }

    public void exitGame()
    {
        Application.Quit();
    }

    public void resetScene()
    { 
        string currentScene = SceneManager.GetActiveScene().name;
        RoomGrid.goalNumber = 0;
        SceneManager.LoadScene(currentScene);
    }

    public void goToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void goToSettings()
    {
        SceneManager.LoadScene("SettingsScene");
    }
}
