﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandMechanic : MonoBehaviour {

    [HideInInspector]
    public GameObject movableObject;
    GameObject room;

    [HideInInspector]
    public int objectIdx;

    private bool spacePressed = false;

    private void Start()
    {
        room = GameObject.Find("Room");
    }

    private void Update()
    {
        if (gameObject.GetComponent<Movement>().input == true)
        {
            if (gameObject.GetComponent<Movement>().haveObject)
            {
                if (Input.GetKeyDown(KeyCode.Space) && !spacePressed) 
                {
                    spacePressed = true;
                    int playerIdx = gameObject.GetComponent<Movement>().currentPositionIndex;
                    int playerColumn = playerIdx % RoomGrid.levelSize;
                    int playerRow = playerIdx / RoomGrid.levelSize;
                    //drop object
                    char objDir = gameObject.GetComponent<Movement>().handOrientation;
                    if (objDir == 'n')
                    {
                        if(playerRow != 0)
                        {
                            if(RoomGrid.levelData[playerIdx - RoomGrid.levelSize] == 1)
                            {
                                dropObject2();
                            }
                        }
                    }
                    else if (objDir == 's')
                    {
                        if (playerRow != RoomGrid.levelSize-1)
                        {
                            if (RoomGrid.levelData[playerIdx + RoomGrid.levelSize] == 1)
                            {
                                dropObject2();
                            }
                        }
                    }
                    else if (objDir == 'e')
                    {
                        if (playerColumn != RoomGrid.levelSize - 1)
                        {
                            if (RoomGrid.levelData[playerIdx + 1] == 1)
                            {
                                dropObject2();
                            }
                        }
                    }
                    else if(objDir == 'w')
                    {
                        if (playerColumn != 0)
                        {
                            if (RoomGrid.levelData[playerIdx - 1] == 1)
                            {
                                dropObject2();
                            }
                        }
                    }
                }
            }
        }

        //if (gameObject.GetComponent<Movement>().haveObject == true) { checkDropObject();}
        spacePressed = false;
    }

    private void OnTriggerStay(Collider _col)
    {
        objectIdx = RoomGrid.levelSize * -Mathf.RoundToInt(_col.gameObject.transform.position.z) + Mathf.RoundToInt(_col.gameObject.transform.position.x);
        
        if(gameObject.GetComponent<Movement>().haveObject == false && (RoomGrid.levelData[objectIdx] == 3 || RoomGrid.levelData[objectIdx] == 4))
        {
            if (gameObject.GetComponent<Movement>().input == true)
            {
                if (Input.GetKeyDown(KeyCode.Space) && !spacePressed)
                {
                    spacePressed = true;
                    //grabs
                    catchObject2(_col);
                    //StartCoroutine(catchObject(_col));
                }
            }
        }
    }

    private void catchObject2(Collider col)
    {
        movableObject = col.gameObject;
        movableObject.transform.parent = transform;
        if (RoomGrid.levelData[objectIdx] == 3) { gameObject.GetComponent<Movement>().objectDataGrid = 3; }
        else { gameObject.GetComponent<Movement>().objectDataGrid = 4; }
        RoomGrid.levelData[objectIdx] = 1;
        
        gameObject.GetComponent<Movement>().haveObject = true;
    }

    private void dropObject2()
    {
        movableObject.transform.parent = null;
        updateObjIdx();
        int objectType = gameObject.GetComponent<Movement>().objectDataGrid;
        RoomGrid.levelData[objectIdx] = objectType;
        gameObject.GetComponent<Movement>().haveObject = false;

        if(objectType == 4)
        {
            if (movableObject.GetComponent<noJoyBehaviour>().isInTrash()) movableObject.GetComponent<noJoyBehaviour>().getTrashed();
            if (RoomGrid.goalNumber == 0)
            {
                room.GetComponent<RoomGrid>().winCanvas.SetActive(true);
                gameObject.GetComponent<Movement>().winner = true;
            }
        }
        movableObject = null;

    }

    public void updateObjIdx()
    {
        objectIdx = RoomGrid.levelSize * -Mathf.RoundToInt(movableObject.transform.position.z) + Mathf.RoundToInt(movableObject.transform.position.x);
    }
}
